# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'settings.ui'
#
# Created: Tue Mar 14 22:39:11 2017
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Settings(object):
    def setupUi(self, Settings):
        Settings.setObjectName(_fromUtf8("Settings"))
        Settings.resize(300, 297)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Settings.sizePolicy().hasHeightForWidth())
        Settings.setSizePolicy(sizePolicy)
        Settings.setMinimumSize(QtCore.QSize(300, 297))
        Settings.setMaximumSize(QtCore.QSize(300, 297))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../img/Settings-L-icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Settings.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(Settings)
        self.verticalLayout.setContentsMargins(9, 0, -1, 0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setSizeConstraint(QtGui.QLayout.SetFixedSize)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.apply_button = QtGui.QPushButton(Settings)
        self.apply_button.setObjectName(_fromUtf8("apply_button"))
        self.gridLayout.addWidget(self.apply_button, 0, 2, 1, 1)
        self.label_8 = QtGui.QLabel(Settings)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.gridLayout.addWidget(self.label_8, 8, 0, 1, 2)
        self.checkBox = QtGui.QCheckBox(Settings)
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.gridLayout.addWidget(self.checkBox, 5, 2, 1, 1)
        self.label_4 = QtGui.QLabel(Settings)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 4, 0, 1, 2)
        self.lineEdit_2 = QtGui.QLineEdit(Settings)
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.gridLayout.addWidget(self.lineEdit_2, 3, 2, 1, 1)
        self.label_5 = QtGui.QLabel(Settings)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 5, 0, 1, 2)
        self.lineEdit_3 = QtGui.QLineEdit(Settings)
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.gridLayout.addWidget(self.lineEdit_3, 4, 2, 1, 1)
        self.label_6 = QtGui.QLabel(Settings)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.gridLayout.addWidget(self.label_6, 6, 0, 1, 2)
        self.lineEdit_4 = QtGui.QLineEdit(Settings)
        self.lineEdit_4.setObjectName(_fromUtf8("lineEdit_4"))
        self.gridLayout.addWidget(self.lineEdit_4, 2, 2, 1, 1)
        self.cancel_button = QtGui.QPushButton(Settings)
        self.cancel_button.setObjectName(_fromUtf8("cancel_button"))
        self.gridLayout.addWidget(self.cancel_button, 0, 0, 1, 2)
        self.horizontalSlider_2 = QtGui.QSlider(Settings)
        self.horizontalSlider_2.setCursor(QtGui.QCursor(QtCore.Qt.SplitHCursor))
        self.horizontalSlider_2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_2.setObjectName(_fromUtf8("horizontalSlider_2"))
        self.gridLayout.addWidget(self.horizontalSlider_2, 8, 2, 1, 1)
        self.checkBox_2 = QtGui.QCheckBox(Settings)
        self.checkBox_2.setObjectName(_fromUtf8("checkBox_2"))
        self.gridLayout.addWidget(self.checkBox_2, 6, 2, 1, 1)
        self.lineEdit = QtGui.QLineEdit(Settings)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout.addWidget(self.lineEdit, 1, 2, 1, 1)
        self.horizontalSlider = QtGui.QSlider(Settings)
        self.horizontalSlider.setCursor(QtGui.QCursor(QtCore.Qt.SplitHCursor))
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName(_fromUtf8("horizontalSlider"))
        self.gridLayout.addWidget(self.horizontalSlider, 7, 2, 1, 1)
        self.label_3 = QtGui.QLabel(Settings)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 3, 0, 1, 2)
        self.label_7 = QtGui.QLabel(Settings)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.gridLayout.addWidget(self.label_7, 7, 0, 1, 2)
        self.label = QtGui.QLabel(Settings)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 1, 0, 1, 2)
        self.label_2 = QtGui.QLabel(Settings)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 2)
        self.verticalLayout.addLayout(self.gridLayout)

        self.retranslateUi(Settings)
        QtCore.QObject.connect(self.cancel_button, QtCore.SIGNAL(_fromUtf8("clicked()")), Settings.close)
        QtCore.QMetaObject.connectSlotsByName(Settings)

    def retranslateUi(self, Settings):
        Settings.setWindowTitle(_translate("Settings", "Dialog", None))
        self.apply_button.setText(_translate("Settings", "Apply", None))
        self.label_8.setText(_translate("Settings", "Pitch: 0.5", None))
        self.checkBox.setText(_translate("Settings", "CheckBox", None))
        self.label_4.setText(_translate("Settings", "Timeout:", None))
        self.label_5.setText(_translate("Settings", "WIFI:", None))
        self.label_6.setText(_translate("Settings", "BLUETOOTH:", None))
        self.cancel_button.setText(_translate("Settings", "Cancel", None))
        self.checkBox_2.setText(_translate("Settings", "CheckBox", None))
        self.label_3.setText(_translate("Settings", "Minimum speed", None))
        self.label_7.setText(_translate("Settings", "Yaw: 5.0", None))
        self.label.setText(_translate("Settings", "IP", None))
        self.label_2.setText(_translate("Settings", "MAC", None))

