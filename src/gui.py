#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Nicola Covallero

Simple GUI for my robot.
A nice tutorial: http://www.python.it/wiki/show/qttutorial/
"""

# https://pythonspot.com/en/pyqt4/

import os, sys
from PyQt4 import QtGui, QtCore
import main
import settingswindow
import connection2robot, receivedata, camerastream
import udpsocket
import bluetooth
import time
from PIL import Image

import socket
import struct

import io
import cv2
import numpy as np




class JRI(QtGui.QMainWindow, main.Ui_MainWindow):
    """
    Main class which handles the GUI. This GUI allows to communicate and control with Johnny Robot
    either via WIFI or via BLUETOOTH. You can control wheels and camera motors, speeds, and see
    the image capture by the robot camera (still to implement).

    Attributes:
        max_speed: maximum wheels speed
        min_speed: minimum wheels speed
        timeout: timeout for the connection WIFI socket
        communication_style: string to specify the communication style (either WIFI or BLUETOOTH)

        --CAMERA VARIABLES--
        yaw_angle
        pitch_angle
        yaw_angle_range
        pitch_angle_range
        yaw_sensibility: how much increase the yaw reference angle on button pression
        pitch_sensibility: how much increase the pitch reference angle on button pression

        --COMMUNICATION VARIABLES--
        CONNECTION_PORT: wifi socket port to establish connection
        DRIVING_PORT: wifi socket port to control wheels motors
        CAMERA_DRIVING_PORT: wifi socket port to control camera motors

        DRIVING_SERVICE_UUID: bluetooth service UUID for controlling the wheels motors
        CAMERA_DRIVING_SERVICE_UUID: bluetooth service UUID for controlling the camera motors

        driving_socket_connected: boolean (True: connected)
        camera_driving_socket_connected: boolean (True: connected)
        connected_to_robot: boolean (True: connected)
        IP: IP robot address
        rasp_mac_addr: MAC address of the robot
    """

    def __init__(self,parent=None):
        """
        Initialization of the GUI and of variables. Here the main variables are defined.
        """

        super(JRI, self).__init__(parent)
        self.setupUi(self)
        # properties
        self.max_speed = 100 
        self.min_speed = 70
        self.timeout = 0.05 # timeout for the socket test conectiona
        self.speed = 0 #
        self.communication_style = 'BLUETOOTH' # WIFI or BLUETOOTH

        # camera variables
        self.yaw_angle = 0
        self.pitch_angle = 0
        self.yaw_angle_range = 180
        self.pitch_angle_range = 180 # this expresses the range, as soon as it is connected to the robot it will get the real values
        self.yaw_sensibility = 5.0 # in degrees
        self.pitch_sensibility = 5.0  # in degrees


        # COMMUNICATIONS --------------------------------------------
        # Socket used only to establish connection with the robot
        self.CONNECTION_PORT = 2525
        #self.CONNECTION_SERVICE_UUID = "94f39d29-7d6d-437d-973b-fba39e49d4ec" # still not used


        self.DRIVING_PORT = 2526
        self.DRIVING_SERVICE_UUID = "94f39d29-7d6d-437d-973b-fba39e49d4ed"
        self.driving_socket_connected = False

        self.CAMERA_DRIVING_PORT = 2529
        self.CAMERA_DRIVING_SERVICE_UUID = "94f39d29-7d6d-437d-973b-fba39e49d4ec"
        self.camera_driving_socket_connected = False

        self.CAMERA_PORT = 8000
        self.camera_connection_started = False
        self.captured_images = 0 # used just to compute the fps
        self.opencv_used = True # specify is use the OpenCV or PIL library to handle the image
        # -----------------------------------------------------------

        if self.communication_style == "WIFI":
            self.driving_socket = udpsocket.UDPSocket()
            self.connection_socket = udpsocket.UDPSocket()
            self.camera_driving_socket = udpsocket.UDPSocket()
        else:
            self.setBluetoothCommunication()

        self.time_last_frame = 0

        self.connected_to_robot = False
        self.IP = "192.168.0.8"
        self.rasp_mac_addr = "00:15:83:E8:49:2D"

        # set properties for the buttons -----------------
        self.w_pressed = False
        self.w_released = True
        self.a_pressed = False
        self.a_released = True
        self.s_pressed = False
        self.s_released = True
        self.d_pressed = False
        self.d_released = True
        self.right_arrow_pressed = False
        self.right_arrow_released = True
        self.left_arrow_pressed = False
        self.left_arrow_released = True
        self.up_arrow_pressed = False
        self.up_arrow_released = True
        self.down_arrow_pressed = False
        self.down_arrow_released = True
        self.minus_pressed = False
        self.minus_released = True
        self.plus_pressed = False
        self.plus_released = True
        # ------------------------------------------------

        # Default image when no image is received
        self.picamera_frame = Image.open('../img/jr_cropped.png').rotate(180, expand = True)
        if self.communication_style == 'WIFI':
            self.connection.setIcon(QtGui.QIcon("../img/wifi-icon-off.png"))
        else:
            self.connection.setIcon(QtGui.QIcon("../img/bluetooth-icon-off.png"))

        # save the threads
        self.threads = []  # <---- IMPORTANT TO PUT


        self.settings.triggered.connect(self.openSettings)
        self.connection.triggered.connect(self.connect)
        self.camera_test.triggered.connect(self.cameraStreaming)
        self.updatePixMap(self.picamera_frame, opencv_used=False)

        # ref: http://stackoverflow.com/questions/29807201/update-qwidget-every-minutes
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(100)  # 200ms of interval
        self.timer.start(0)

        self.pic.resize(640,480)
        self.sld.setMinimum(self.min_speed)
        self.sld.setValue((self.max_speed + self.min_speed)/2)
        self.velocity_label.setText("Velocity: " + str(self.sld.value()))
        self.yaw_label.setText('YAW: ' + str(self.yaw_angle) + " +/-" + str(self.yaw_angle_range/2))
        self.pitch_label.setText('Pitch: ' + str(self.pitch_angle) + " +/-" + str(self.pitch_angle_range / 2))
        # --- connections ---
        self.timer.timeout.connect(self.updateImage)
        self.timer.timeout.connect(self.updateKeys)
        self.sld.valueChanged[int].connect(self.changeValue)


    def cameraStreaming(self):
        # camera_stream = camerastream.CameraStream(self)
        # camera_stream.connection_done.connect(self.update_connection) # this is called when a signal is emitted by such a thread
        # self.threads.append(camera_stream)
        # camera_stream.start() # start the thread

        # for the moment only wifi
        try:
            self.camera_client_socket = socket.socket()
            self.print_("Connecting to raspberry's camera port")
            self.camera_client_socket.connect((self.IP, self.CAMERA_PORT))
            self.print_("Connected to raspberry's camera port")   
        except socket.error, msg:
            self.print_(' Camera Streaming Test: Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
            return

        self.camera_connection = self.camera_client_socket.makefile('wb')
        self.camera_connection_started = True

    def sendBluetoothData(self, sock, data):
        """
        Handle the sending of data via Bluetooth

        Args:
            sock: bluetooth socket (class bluetooth.socket )
            data: string to send
            ID:

        Returns:

        """
        if not self.communication_style == "WIFI":
            try:
                sock.send(data)
            except:
                self.updateConnection() # update connection label
                self.setBluetoothCommunication() # reset
        else:
            pass

    def updateConnection(self, val = False):
        """
        Update the logo of the current type connection accordingly to the boolean 'val' value.
        If the connection type is WIFI the logo is set to green if val = True.
        Args:
            val: boolean value (OFF not connected/RED logo - ON connected/GREEN logo)

        Returns:

        """
        if not val:
            self.connected_to_robot = False
            if self.communication_style == "WIFI":
                self.connection.setIcon(QtGui.QIcon('../img/wifi-icon-off'))
            else:
                self.connection.setIcon(QtGui.QIcon('../img/bluetooth-icon-off'))
        else:
            self.connected_to_robot = True
            if self.communication_style == "WIFI":
                self.connection.setIcon(QtGui.QIcon('../img/wifi-icon-on'))
            else:
                self.connection.setIcon(QtGui.QIcon('../img/bluetooth-icon-on'))

    def changeValue(self, value):
        """
           Change value of the speed accordingly to the slidebar
        Args:
            value:

        Returns:

        """
        str_ = 'Velocity: \n' + str(value)
        self.speed = value
        self.velocity_label.setText(str_)

    def connect(self):
        """
        Connect to the robot via the communication type set.
        Returns:

        """
        self.updateConnection()
        connection = connection2robot.ConnectToRobot(self)
        connection.connection_done.connect(self.update_connection) # this is called when a signal is emitted by such a thread
        self.threads.append(connection)
        connection.start() # start the thread



    def closeEvent(self, event):# this is the reimplementation of the default closeEvent of QtGui
        """
        Message window before to close
        :param event:
        :return:
        """
        reply = QtGui.QMessageBox.question(self, 'Message',
                                           "Are you sure to quit?", QtGui.QMessageBox.Yes |
                                           QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            # send the motors to the original position
            if self.communication_style == 'WIFI':
                self.camera_driving_socket.sendData("yaw/" + str(0.0) + "/"+"pitch/" + str(0.0), IP=self.IP,
                                                    PORT=self.CAMERA_DRIVING_PORT)
            else:
                if self.driving_socket_connected:
                    self.driving_socket.close()
                if self.camera_driving_socket_connected:
                    self.sendBluetoothData(self.camera_driving_socket,"yaw/" + str(0.0) + "/" + "pitch/" + str(0.0))
                    #self.camera_driving_socket.send("yaw/" + str(0.0) + "/" + "pitch/" + str(0.0))
                    self.camera_driving_socket.close()
                                                                                                                     
            QtCore.QCoreApplication.instance().quit()

    def closeButtonEvent(self, event):  # this is the reimplementation of the default closeEvent of QtGui
        """
        Close button event.
        :param event:
        :return:
        """
        reply = QtGui.QMessageBox.question(self, 'Message',
                                           "Are you sure to quit?", QtGui.QMessageBox.Yes |
                                           QtGui.QMessageBox.No, QtGui.QMessageBox.No)
        

        if reply == QtGui.QMessageBox.Yes:  
            # send the motors to the original position
            if self.communication_style == 'WIFI':
                self.camera_driving_socket.sendData("yaw/" + str(0.0) + "/"+"pitch/" + str(0.0), IP=self.IP,
                                                    PORT=self.CAMERA_DRIVING_PORT)
            else:
                if self.driving_socket_connected:
                    self.driving_socket.close()
                if self.camera_driving_socket_connected:
                    self.sendBluetoothData(self.camera_driving_socket,"yaw/" + str(0.0) + "/" + "pitch/" + str(0.0))
                    #self.camera_driving_socket.send("yaw/" + str(0.0) + "/" + "pitch/" + str(0.0))
                    self.camera_driving_socket.close()

            QtCore.QCoreApplication.instance().quit()


    def center(self):
        """
        Center the window in the display
        :return:
        """

        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def updateImageCV(self):
        """
        Update the image in the case the image is provided as IplImage (only for test this will be deleted)
        :return:
        """
        self.frame = cv.QueryFrame(self.capture)
        self.resize_frame = cv.CreateMat(480, 640, cv.CV_8UC3)
        cv.Resize(self.frame, self.resize_frame)
        image = QtGui.QImage(self.resize_frame.tostring(), self.resize_frame.width, self.resize_frame.height, QtGui.QImage.Format_RGB888).rgbSwapped()

        pixmap = QtGui.QPixmap.fromImage(image)
        self.pic.setPixmap(pixmap)

    # Old piece of code
    # def updateImage(self):
    #     """
    #     Update the image
    #     :return:
    #     """
    #
    #     #self.counter = self.counter + 1
    #     #print "updating Image", self.counter
    #
    #     # silly thing just to see how fast is, changing mage each iteration
    #
    #     #if self.image == 1:
    #     #    pi = Image.open('img/tux.jpg')
    #     #    self.image = 2
    #     #else:
    #     #    pi = Image.open('img/jri_logo.png')
    #     #    self.image = 1
    #
    #     # we skip the conversion process by transforming the image directly from the QImage
    #     #self.frame = cv.fromarray(numpy.asarray(pi).astype(numpy.uint8))
    #     #print "frame size", self.frame.channels
    #     #self.resize_frame = cv.CreateMat(480, 640, self.frame.type)#
    #     #cv.Resize(self.frame, self.resize_frame)
    #     #self.resize_frame =  numpy.array(self.resize_frame)
    #
    #     # print self.resize_frame.shape
    #     time_ = time.time()
    #     a = numpy.asarray(self.picamera_frame).astype(numpy.uint32)
    #     # we know pack the RGB values (also the Alpha channel if it exists (PNG))
    #     # They have to be pucked accordingly to the Format of the QImage, in pour case ARGB32
    #     # which means 32 bits where: 8 bites (alpha) - 8 bites (red) - 8 bites (green) - 8 bites (blue)
    #     # if the file has no alpha is better to put a fake one.
    #     if a.shape[2] == 4:
    #         b = (  a[:, :, 3] << 24 | a[:, :, 0] << 16 | a[:, :, 1] << 8 | a[:, :, 2]).flatten()
    #     else:
    #         b = ( 255 << 24 | a[:, :, 0] << 16 | a[:, :, 1] << 8 | a[:, :, 2]).flatten()
    #     image = QtGui.QImage(b, a.shape[1], a.shape[0], QtGui.QImage.Format_ARGB32)
    #     size = QtCore.QSize(640,480)
    #     image = image.scaled(size)
    #     #self.print_("Conversion time" + str(time.time() - time_))
    #
    #     #
    #     # image = QtGui.QImage(self.resize_frame.shape[1],self.resize_frame.shape[0], QtGui.QImage.Format_RGB888)
    #     #
    #     # for x in range(0,self.resize_frame.shape[1]):
    #     #     for y in range(0, self.resize_frame.shape[0]):
    #     #         c = QtGui.QColor(self.resize_frame[y][x][0],self.resize_frame[y][x][1],self.resize_frame[y][x][2])
    #     #         image.setPixel(x,y,c.rgb())
    #     # print "To convert the image took", time.time() - time_
    #
    #     # the folliwng two lines are only usefull to check that the image is actually good, and that the problem is only during the QImage
    #     #cv2.imshow('image', a.astype(numpy.uint8))
    #     #cv2.waitKey()
    #
    #     pixmap = QtGui.QPixmap.fromImage(image)
    #
    #     self.pic.setPixmap(pixmap)

    def updateImage(self):
        """
        Read the image from socket and update the pixmap
        Returns:

        """
        if self.camera_connection_started:
            connection = self.camera_connection

            if self.captured_images == 0:
                self.capture_start_time = time.time()
            elif self.captured_images == 10:#update the FPS label every 10 images received
                self.camera_fps_label.setText('FPS: %d.0' % (10 / (time.time() - self.capture_start_time)))
                self.captured_images = 0
                self.capture_start_time = time.time()

            try:

                # Read the length of the image as a 32-bit unsigned int. If the
                # length is zero, quit the loop
                image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]

                if not image_len:
                    self.camera_connection_started = False

                # Construct a stream to hold the image data and read the image
                # data from the connection
                image_stream = io.BytesIO()
                image_stream.write(connection.read(image_len))
                # Rewind the stream, open it as an image with PIL and do some
                # processing on it
                image_stream.seek(0)

                if not self.opencv_used:
                    self.picamera_frame = Image.open(image_stream)
                    self.camera_size_label.setText('Image is %dx%d' % self.picamera_frame.size)
                    self.updatePixMap(self.picamera_frame)
                else:
                    # Construct a numpy array from the stream
                    data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
                    # "Decode" the image from the array, preserving colour
                    image = cv2.imdecode(data, 1)
                    # OpenCV returns an array with data in BGR order. If you want RGB instead
                    # use the following...
                    # image = image[:; :, ::-1]
                    self.picamera_frame = image
                    self.updatePixMap(self.picamera_frame)

                self.captured_images = self.captured_images + 1
            except socket.error, msg:
                self.camera_connection_started = False
                self.print_(' Camera Streaming Test: Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
            #except:
            #    print "exeception2"
            #    self.camera_connection_started = False




                # finally:
                #     connection.close()
                #     self.camera_client_socket.close()

    def updatePixMap(self, frame, rotate = True, opencv_used = True):
        """
        Update the camera's pixmap to show the image. The image has to be turned by 180 degrees. The image can
        be handled either with OpenCV library or with PIL.
        Args:
            frame: frame to show in the pixmap
            rotate: true to rotate the frame by 180 degrees
            opencv_used: true if use the OpenCV library (slightly faster than PIL) or False to use PIL

        Returns: None

        """
        if not opencv_used:
            a = np.asarray(self.picamera_frame).astype(np.uint32)
            # we know pack the RGB values (also the Alpha channel if it exists (PNG))
            # They have to be pucked accordingly to the Format of the QImage, in pour case ARGB32
            # which means 32 bits where: 8 bites (alpha) - 8 bites (red) - 8 bites (green) - 8 bites (blue)
            # if the file has no alpha is better to put a fake one.
            if a.shape[2] == 4:
                b = (a[:, :, 3] << 24 | a[:, :, 0] << 16 | a[:, :, 1] << 8 | a[:, :, 2]).flatten()
            else:
                b = (255 << 24 | a[:, :, 0] << 16 | a[:, :, 1] << 8 | a[:, :, 2]).flatten()
            image = QtGui.QImage(b, a.shape[1], a.shape[0], QtGui.QImage.Format_ARGB32)
            size = QtCore.QSize(640, 480)
            image = image.scaled(size)

            if rotate:
                pixmap = QtGui.QPixmap.fromImage(image).transformed(QtGui.QTransform().rotate(180))
            else:
                pixmap = QtGui.QPixmap.fromImage(image)

            self.pic.setPixmap(pixmap)

        else:
            # the new cv2 interface for Python integrates numpy arrays into the OpenCV framework, which
            # makes operations much simpler
            #resize_frame = cv.CreateMat(480, 640, cv.CV_8UC3)
            resize_frame = cv2.resize(frame, (480,640) )
            #image = QtGui.QImage(resize_frame.tostring(), resize_frame.width, resize_frame.height,
            #                     QtGui.QImage.Format_RGB888).rgbSwapped()
            image = QtGui.QImage(resize_frame.tostring(), resize_frame.shape[1], resize_frame.shape[0],
                      QtGui.QImage.Format_RGB888).rgbSwapped()
            if rotate:
                pixmap = QtGui.QPixmap.fromImage(image).transformed(QtGui.QTransform().rotate(180))
            else:
                pixmap = QtGui.QPixmap.fromImage(image)

            self.pic.setPixmap(pixmap)

    # TODO: detect multiple key pressing, this method is able tod etect one pressing at a time
    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_W:
            #self.print_('pressed W')
            self.w_pressed = True
            self.w_released = False
        if e.key() == QtCore.Qt.Key_S:
            # self.print_( 'pressed S')
            self.s_pressed = True
            self.s_released = False
        if e.key() == QtCore.Qt.Key_A:
            # self.print_( 'pressed A')
            self.a_pressed = True
            self.a_released = False
        if e.key() == QtCore.Qt.Key_D:
            # self.print_( 'pressed D')
            self.d_pressed = True
            self.d_released = False
        if e.key() == QtCore.Qt.Key_Minus:
            # self.print_( 'Reducing speed')
            self.minus_pressed = True
            self.minus_released = False
            self.sld.setValue(self.sld.value() - 1)
        if e.key() == QtCore.Qt.Key_Plus:
            # self.print_( 'Increasing speed')
            self.plus_pressed = True
            self.plus_released = False
            self.sld.setValue(self.sld.value() + 1)
        if e.key() == QtCore.Qt.Key_Up:
            self.up_arrow_pressed = True
            self.up_arrow_released = False
            self.pitch_angle = self.pitch_angle + self.pitch_sensibility
            self.pitchSaturation()
            if self.communication_style == 'WIFI':
                self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle),
                                                IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
            else:
                self.sendBluetoothData(self.camera_driving_socket,"yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))
                #self.camera_driving_socket.send("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))

            self.pitch_label.setText('PITCH: ' + str(self.pitch_angle) + " +/-" + str(self.pitch_angle_range / 2))
        if e.key() == QtCore.Qt.Key_Down:
            self.down_arrow_pressed = True
            self.down_arrow_released = False
            self.pitch_angle = self.pitch_angle - self.pitch_sensibility
            self.pitchSaturation()
            if self.communication_style == 'WIFI':
                self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle),
                                                IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
            else:
                self.sendBluetoothData(self.camera_driving_socket,"yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))
                #self.camera_driving_socket.send("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))

            self.pitch_label.setText('PITCH: ' + str(self.pitch_angle) + " +/-" + str(self.pitch_angle_range / 2))
        if e.key() == QtCore.Qt.Key_Right:
            self.right_arrow_pressed = True
            self.right_arrow_released = False
            self.yaw_angle = self.yaw_angle + self.yaw_sensibility
            self.yawSaturation()
            if self.communication_style == 'WIFI':
                self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle),
                                                IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
            else:
                self.sendBluetoothData(self.camera_driving_socket,"yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))
                #self.camera_driving_socket.send("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))

            self.yaw_label.setText('YAW: ' + str(self.yaw_angle) + " +/-" + str(self.yaw_angle_range / 2))
        if e.key() == QtCore.Qt.Key_Left:
            self.left_arrow_pressed = True
            self.left_arrow_released = False
            self.yaw_angle = self.yaw_angle - self.yaw_sensibility
            self.yawSaturation()
            if self.communication_style == 'WIFI':
                self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle),
                                                IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
            else:
                self.sendBluetoothData(self.camera_driving_socket,"yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))
                #self.camera_driving_socket.send("yaw/" + str(self.yaw_angle) + "/" + "pitch/" + str(self.pitch_angle))

            self.yaw_label.setText('YAW: ' + str(self.yaw_angle) + " +/-" + str(self.yaw_angle_range/2))

    def keyReleaseEvent(self, e):
        if e.key() == QtCore.Qt.Key_W:
            self.w_pressed = False
            self.w_released = True
        if e.key() == QtCore.Qt.Key_S:
            self.s_pressed = False
            self.s_released = True
        if e.key() == QtCore.Qt.Key_A:
            self.a_pressed = False
            self.a_released = True
        if e.key() == QtCore.Qt.Key_D:
            self.d_pressed = False
            self.d_released = True
        if e.key() == QtCore.Qt.Key_Minus:
            self.minus_pressed = False
            self.minus_released = True
        if e.key() == QtCore.Qt.Key_Plus:
            self.plus_pressed = False
            self.plus_released = True
        if e.key() == QtCore.Qt.Key_Up:
            self.up_arrow_pressed = False
            self.up_arrow_released = True
        if e.key() == QtCore.Qt.Key_Down:
            self.down_arrow_pressed = False
            self.down_arrow_released = True
        if e.key() == QtCore.Qt.Key_Right:
            self.right_arrow_pressed = False
            self.right_arrow_released = True
        if e.key() == QtCore.Qt.Key_Left:
            self.left_arrow_pressed = False
            self.left_arrow_released = True

    def updateKeys(self):
        if self.w_pressed and not self.w_released:
            #self.print_('W pressed')
            if self.connected_to_robot:
                if self.communication_style == 'WIFI':
                    self.driving_socket.sendData("forward-"+str(self.speed), IP=self.IP, PORT=self.DRIVING_PORT)
                else:
                    self.sendBluetoothData(self.driving_socket,"forward-" + str(self.speed))
                    #self.driving_socket.send("forward-" + str(self.speed))
        if self.a_pressed and not self.a_released:
            #self.print_('A pressed')
            if self.connected_to_robot:
                if self.communication_style == 'WIFI':
                    self.driving_socket.sendData("left-" + str(self.speed), IP=self.IP, PORT=self.DRIVING_PORT)
                else:
                    self.sendBluetoothData(self.driving_socket,"left-" + str(self.speed))
                    #self.driving_socket.send("left-" + str(self.speed))
        if self.d_pressed and not self.d_released:
            #self.print_('D pressed')
            if self.connected_to_robot:
                if self.communication_style == 'WIFI':
                    self.driving_socket.sendData("right-" + str(self.speed), IP=self.IP, PORT=self.DRIVING_PORT)
                else:
                    self.sendBluetoothData(self.driving_socket,"right-" + str(self.speed))
                    #self.driving_socket.send("right-" + str(self.speed))
        if self.s_pressed and not self.s_released:
            if self.connected_to_robot:
                if self.communication_style == 'WIFI':
                    self.driving_socket.sendData("backward-" + str(self.speed), IP=self.IP, PORT=self.DRIVING_PORT)
                else:
                    self.sendBluetoothData(self.driving_socket,"backward-" + str(self.speed))
                    #self.driving_socket.send("backward-" + str(self.speed))
        # if self.plus_pressed and not self.plus_released:
        #
        #     self.sld.setValue(self.sld.value() + 1)
        # if self.minus_pressed and not self.minus_released:
        #     self.sld.setValue(self.sld.value() - 1)

        # CAMERA DRIVING MOTORS
        # if self.up_arrow_pressed and not self.up_arrow_released:
        #     #self.print_("Up pressed")
        #     if self.connected_to_robot:
        #         self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) +"/"+"pitch/"+str(self.pitch_angle), IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
        # if self.down_arrow_pressed and not self.down_arrow_released:
        #     #self.print_("Down pressed")
        #     if self.connected_to_robot:
        #         self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) +"/"+"pitch/"+str(self.pitch_angle), IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
        # if self.right_arrow_pressed and not self.right_arrow_released:
        #     #self.print_("Right pressed")
        #     if self.connected_to_robot:
        #         self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) +"/"+"pitch/"+str(self.pitch_angle), IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
        # if self.left_arrow_pressed and not self.left_arrow_released:
        #     #self.print_("Left pressed")
        #     if self.connected_to_robot:
        #         self.camera_driving_socket.sendData("yaw/" + str(self.yaw_angle) +"/"+"pitch/"+str(self.pitch_angle), IP=self.IP, PORT=self.CAMERA_DRIVING_PORT)
        # -------------------------------

    @QtCore.pyqtSlot()
    def openSettings(self):
        self.settingsWindow = settingswindow.SettingsWindow(self) # This is important, in this way
        # we reset the window, otherwise if we open again the window we are not creating a new class but
        # visualize the window of before, which has been hidden. 
        self.settingsWindow.IP_input.setText(str(self.IP))
        self.settingsWindow.MAC_input.setText(str(self.rasp_mac_addr))
        self.settingsWindow.exec_()

        # update
        if self.communication_style == 'WIFI':
            img = '../img/wifi-icon-'
            if self.connected_to_robot:
                self.connection.setIcon(QtGui.QIcon(img + 'on'))
                self.connection.setToolTip("Connect to the robot - ON")
            else:
                self.connection.setIcon(QtGui.QIcon(img + 'off'))
                self.connection.setToolTip("Connect to the robot - OFF")
            self.setWIFICommunication()
        elif self.communication_style == 'BLUETOOTH':
            img = '../img/bluetooth-icon-'
            if self.connected_to_robot:
                self.connection.setIcon(QtGui.QIcon(img + 'on'))
                self.connection.setToolTip("Connect to the robot - ON")
            else:
                self.connection.setIcon(QtGui.QIcon(img + 'off'))
                self.connection.setToolTip("Connect to the robot - OFF")
            self.setBluetoothCommunication()
        else:
            self.print_('ERROR: unrecognized communication style: ' + str(self.communication_style))

        pass

    def getData(self):
        """
        Get data from the raspbarry (Sonar and Camera frame)
        :return:
        """
        # TODO these two sockets are the same and are waiting for different data, we should use just one
        # and add a code to specify the kind of data it has been sent
        # When we send a data we can specify the kind of port (In the rasp the socket has been binded)
        # We could do here a similar thing creating two sockets only for riceiving
        # http://stackoverflow.com/questions/8994937/send-image-using-socket-programming-python

        self.threads = [] # <---- IMPORTANT TO PUT
        receiveDataSonar = receivedata.ReceiveData(self.connection_socket, self.timeout)
        receiveDataSonar.data_received.connect(self.data_received_sonar)
        self.threads.append(receiveDataSonar)
        #receiveDataSonar.start()

        receiveDatacamera = ReceiveData(self.connection_socket, self.timeout, 40 * 8192)
        receiveDatacamera.data_received.connect(self.data_received_camera)
        self.threads.append(receiveDatacamera)
        receiveDatacamera.start()

    def data_received_sonar(self,data):
        if self.IP == data[1][0]:
            self.sonar_dist = float(data)

    def data_received_camera(self, data):
        if self.IP == data[1][0]:
            print "received data"
            #self.picamera_frame = cv.fromarray(data)
            self.picamera_frame = data
            self.time_last_frame = time.time()

    def update_connection(self, data):
        """
        Update connection logo
        Args:
            data: boolean (True is the connection is established)

        Returns:

        """
        self.connected_to_robot = data
        print data
        if data:
            self.connection.setToolTip("Connect to the robot - ON")
            if self.communication_style == 'WIFI':
                self.connection.setIcon(QtGui.QIcon('../img/wifi-icon-on'))
            elif self.communication_style == 'BLUETOOTH':
                self.connection.setIcon(QtGui.QIcon('../img/bluetooth-icon-on'))
        else:
            self.connection.setToolTip("Connect to the robot - OFF")
            if self.communication_style == 'WIFI':
                self.connection.setIcon(QtGui.QIcon('../img/wifi-icon-off'))
            elif self.communication_style == 'BLUETOOTH':
                self.connection.setIcon(QtGui.QIcon('../img/bluetooth-icon-off'))
        self.pitch_label.setText('PITCH: ' + str(self.pitch_angle) + " +/-" + str(self.pitch_angle_range / 2))
        self.yaw_label.setText('YAW: ' + str(self.yaw_angle) + " +/-" + str(self.yaw_angle_range / 2))

    def print_(self,text, terminal = True):
        """
        Print text in the terminal and in the textlog as well
        :param text:
        :param terminal: False if you do not want to print in the terminal
        :return:
        """
        if terminal: print text
        self.textLog.append(text)
        self.textLog.moveCursor(QtGui.QTextCursor.End)

    def yawSaturation(self):
        if self.yaw_angle >= self.yaw_angle_range/2:
            self.yaw_angle = self.yaw_angle_range / 2
        if self.yaw_angle <= - self.yaw_angle_range / 2:
            self.yaw_angle = - self.yaw_angle_range / 2

    def pitchSaturation(self):
        if self.pitch_angle >= self.pitch_angle_range / 2:
            self.pitch_angle = self.pitch_angle_range / 2
        if self.pitch_angle <= - self.pitch_angle_range / 2:
            self.pitch_angle = - self.pitch_angle_range / 2

    def setBluetoothCommunication(self):
        """
        Create new bluetooth socket (reset)
        Returns:

        """
        # COMMUNICATIONS --------------------------------------------
        # Socket used only to establish connection with the robot
        self.connection_socket = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        #self.connection_socket.bind(("", bluetooth.PORT_ANY))
        #self.connection_socket.listen(1)

        self.driving_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        #self.driving_socket.bind(("", bluetooth.PORT_ANY))
        #self.driving_socket.listen(1)

        self.camera_driving_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        #self.camera_driving_socket.bind(("", bluetooth.PORT_ANY))
        #self.camera_driving_socket.listen(1)

        # -----------------------------------------------------------

    def setWIFICommunication(self):
        """
        Create new wifi socket (reset)
        Returns:

        """
        self.driving_socket = udpsocket.UDPSocket()
        self.connection_socket = udpsocket.UDPSocket()
        self.camera_driving_socket = udpsocket.UDPSocket()


if __name__ == '__main__':
    try:
        app = QtGui.QApplication(sys.argv)
        ex = JRI()
        ex.show()
        sys.exit(app.exec_())# loop
    except KeyboardInterrupt:
        sys.exit(0)
