# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'settings.ui'
#
# Created: Wed Mar 15 21:02:45 2017
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Settings(object):
    def setupUi(self, Settings):
        Settings.setObjectName(_fromUtf8("Settings"))
        Settings.resize(300, 297)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Settings.sizePolicy().hasHeightForWidth())
        Settings.setSizePolicy(sizePolicy)
        Settings.setMinimumSize(QtCore.QSize(300, 297))
        Settings.setMaximumSize(QtCore.QSize(300, 297))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../img/Settings-L-icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Settings.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(Settings)
        self.verticalLayout.setContentsMargins(9, 0, -1, 0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setSizeConstraint(QtGui.QLayout.SetFixedSize)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.apply_btn = QtGui.QPushButton(Settings)
        self.apply_btn.setObjectName(_fromUtf8("apply_btn"))
        self.gridLayout.addWidget(self.apply_btn, 0, 2, 1, 1)
        self.pitch_label = QtGui.QLabel(Settings)
        self.pitch_label.setObjectName(_fromUtf8("pitch_label"))
        self.gridLayout.addWidget(self.pitch_label, 8, 0, 1, 2)
        self.wifi_checkbox = QtGui.QCheckBox(Settings)
        self.wifi_checkbox.setText(_fromUtf8(""))
        self.wifi_checkbox.setObjectName(_fromUtf8("wifi_checkbox"))
        self.gridLayout.addWidget(self.wifi_checkbox, 5, 2, 1, 1)
        self.timeout_label = QtGui.QLabel(Settings)
        self.timeout_label.setObjectName(_fromUtf8("timeout_label"))
        self.gridLayout.addWidget(self.timeout_label, 4, 0, 1, 2)
        self.min_speed_input = QtGui.QLineEdit(Settings)
        self.min_speed_input.setObjectName(_fromUtf8("min_speed_input"))
        self.gridLayout.addWidget(self.min_speed_input, 3, 2, 1, 1)
        self.wifi_checkbox_label = QtGui.QLabel(Settings)
        self.wifi_checkbox_label.setObjectName(_fromUtf8("wifi_checkbox_label"))
        self.gridLayout.addWidget(self.wifi_checkbox_label, 5, 0, 1, 2)
        self.timeout_input = QtGui.QLineEdit(Settings)
        self.timeout_input.setObjectName(_fromUtf8("timeout_input"))
        self.gridLayout.addWidget(self.timeout_input, 4, 2, 1, 1)
        self.bluetooth_checkbox_label = QtGui.QLabel(Settings)
        self.bluetooth_checkbox_label.setObjectName(_fromUtf8("bluetooth_checkbox_label"))
        self.gridLayout.addWidget(self.bluetooth_checkbox_label, 6, 0, 1, 2)
        self.MAC_input = QtGui.QLineEdit(Settings)
        self.MAC_input.setObjectName(_fromUtf8("MAC_input"))
        self.gridLayout.addWidget(self.MAC_input, 2, 2, 1, 1)
        self.cancel_btn = QtGui.QPushButton(Settings)
        self.cancel_btn.setObjectName(_fromUtf8("cancel_btn"))
        self.gridLayout.addWidget(self.cancel_btn, 0, 0, 1, 2)
        self.pitch_sld = QtGui.QSlider(Settings)
        self.pitch_sld.setCursor(QtGui.QCursor(QtCore.Qt.SplitHCursor))
        self.pitch_sld.setMinimum(1)
        self.pitch_sld.setMaximum(20)
        self.pitch_sld.setOrientation(QtCore.Qt.Horizontal)
        self.pitch_sld.setTickPosition(QtGui.QSlider.TicksAbove)
        self.pitch_sld.setTickInterval(1)
        self.pitch_sld.setObjectName(_fromUtf8("pitch_sld"))
        self.gridLayout.addWidget(self.pitch_sld, 8, 2, 1, 1)
        self.bluetooth_checkbox = QtGui.QCheckBox(Settings)
        self.bluetooth_checkbox.setText(_fromUtf8(""))
        self.bluetooth_checkbox.setObjectName(_fromUtf8("bluetooth_checkbox"))
        self.gridLayout.addWidget(self.bluetooth_checkbox, 6, 2, 1, 1)
        self.IP_input = QtGui.QLineEdit(Settings)
        self.IP_input.setObjectName(_fromUtf8("IP_input"))
        self.gridLayout.addWidget(self.IP_input, 1, 2, 1, 1)
        self.yaw_sld = QtGui.QSlider(Settings)
        self.yaw_sld.setCursor(QtGui.QCursor(QtCore.Qt.SplitHCursor))
        self.yaw_sld.setMinimum(1)
        self.yaw_sld.setMaximum(20)
        self.yaw_sld.setOrientation(QtCore.Qt.Horizontal)
        self.yaw_sld.setTickPosition(QtGui.QSlider.TicksAbove)
        self.yaw_sld.setTickInterval(1)
        self.yaw_sld.setObjectName(_fromUtf8("yaw_sld"))
        self.gridLayout.addWidget(self.yaw_sld, 7, 2, 1, 1)
        self.min_speed_label = QtGui.QLabel(Settings)
        self.min_speed_label.setObjectName(_fromUtf8("min_speed_label"))
        self.gridLayout.addWidget(self.min_speed_label, 3, 0, 1, 2)
        self.yaw_label = QtGui.QLabel(Settings)
        self.yaw_label.setObjectName(_fromUtf8("yaw_label"))
        self.gridLayout.addWidget(self.yaw_label, 7, 0, 1, 2)
        self.IP_label = QtGui.QLabel(Settings)
        self.IP_label.setObjectName(_fromUtf8("IP_label"))
        self.gridLayout.addWidget(self.IP_label, 1, 0, 1, 2)
        self.MAC_label = QtGui.QLabel(Settings)
        self.MAC_label.setObjectName(_fromUtf8("MAC_label"))
        self.gridLayout.addWidget(self.MAC_label, 2, 0, 1, 2)
        self.verticalLayout.addLayout(self.gridLayout)

        self.retranslateUi(Settings)
        QtCore.QObject.connect(self.cancel_btn, QtCore.SIGNAL(_fromUtf8("clicked()")), Settings.close)
        QtCore.QMetaObject.connectSlotsByName(Settings)

    def retranslateUi(self, Settings):
        Settings.setWindowTitle(_translate("Settings", "Dialog", None))
        self.apply_btn.setText(_translate("Settings", "Apply", None))
        self.pitch_label.setText(_translate("Settings", "Pitch: 0.5", None))
        self.timeout_label.setToolTip(_translate("Settings", "Minimum PWM value to make the motors work. This should be tune for each Robot (pair of motors )", None))
        self.timeout_label.setText(_translate("Settings", "Timeout:", None))
        self.min_speed_input.setToolTip(_translate("Settings", "Minimum PWM value to make the motors work. This should be tune for each Robot (pair of motors )", None))
        self.wifi_checkbox_label.setToolTip(_translate("Settings", "Establish the connection with the robot via WIFI", None))
        self.wifi_checkbox_label.setText(_translate("Settings", "WIFI:", None))
        self.timeout_input.setToolTip(_translate("Settings", "Minimum PWM value to make the motors work. This should be tune for each Robot (pair of motors )", None))
        self.bluetooth_checkbox_label.setToolTip(_translate("Settings", "Establish the connection with the robot via bluetooth", None))
        self.bluetooth_checkbox_label.setText(_translate("Settings", "BLUETOOTH:", None))
        self.MAC_input.setToolTip(_translate("Settings", "MAC address of the raspberry, leave it empty if you do not know the correct address and look for all the devices.", None))
        self.cancel_btn.setText(_translate("Settings", "Cancel", None))
        self.min_speed_label.setToolTip(_translate("Settings", "Minimum PWM value to make the motors work. This should be tune for each Robot (pair of motors )", None))
        self.min_speed_label.setText(_translate("Settings", "Minimum speed:", None))
        self.yaw_label.setText(_translate("Settings", "Yaw: 5.0", None))
        self.IP_label.setText(_translate("Settings", "IP:", None))
        self.MAC_label.setToolTip(_translate("Settings", "MAC address of the raspberry, leave it empty if you do not know the correct address and look for all the devices.", None))
        self.MAC_label.setText(_translate("Settings", "MAC:", None))

