#!/usr/bin/python

"""
Thread to establish the connection with the robot
"""
__author__ = 'Nicola Covallero'

from PyQt4 import QtCore

import io
import socket
import struct
import time
from PIL import Image

class CameraStream(QtCore.QThread):
    # reference: http://stackoverflow.com/questions/9957195/updating-gui-elements-in-multithreaded-pyqt
    connection_done = QtCore.pyqtSignal(object)  # this is a signal

    def __init__(self, GUI):
        """
        :param GUI: the main window
        """
        QtCore.QThread.__init__(self)
        self.GUI = GUI  # main window

    def run(self):
        # for the moment only wifi
        try:
            client_socket = socket.socket()
            self.GUI.print_("Connecting to raspberry's camera port")
            client_socket.connect((self.GUI.IP, self.GUI.CAMERA_PORT))
        except socket.error, msg:
            self.GUI.print_(' Camera Streaming Test: Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
            return

        connection = client_socket.makefile('wb')
        connection_started = False
        num_images = 0
        try:
            while True:
                t = time.time()
                # Read the length of the image as a 32-bit unsigned int. If the
                # length is zero, quit the loop
                image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
                if not connection_started:
                    connection_started = True
                    start_time = time.time()

                if not image_len:
                    break

                # Construct a stream to hold the image data and read the image
                # data from the connection
                image_stream = io.BytesIO()
                image_stream.write(connection.read(image_len))
                # Rewind the stream, open it as an image with PIL and do some
                # processing on it
                image_stream.seek(0)

                try:
                    self.GUI.picamera_frame = Image.open(image_stream)
                    self.GUI.updateImage()
                except:
                    pass

                #image.verify()
                num_images = num_images + 1

                self.GUI.print_(('Image is %dx%d' % self.GUI.picamera_frame.size) + " fps: " + str(1/(time.time() - t)))


        finally:
            connection.close()
            client_socket.close()