from __future__ import division
from PyQt4 import QtGui
import sys
import settings
import os

class SettingsWindow(QtGui.QDialog, settings.Ui_Settings):
    def __init__(self,parent=None):
        super(SettingsWindow,self).__init__(parent)
        self.setupUi(self)
        self.IP = ""
        self.parent = parent
        self.pitch_sensibility = self.parent.pitch_sensibility
        self.yaw_sensibility = self.parent.yaw_sensibility
    
        self.IP_input.setText(str(parent.IP))
        self.MAC_input.setText(str(parent.rasp_mac_addr))
        self.min_speed_input.setText(str(parent.min_speed))
        self.timeout_input.setText(str(parent.timeout))


        # connections
        self.apply_btn.clicked.connect(self.applyChanges)
        self.bluetooth_checkbox.clicked.connect(self.BLUETOOTHCheckboxClicked)
        self.wifi_checkbox.clicked.connect(self.WIFICheckboxClicked)

        self.bluetooth_checkbox.setChecked(self.parent.communication_style == 'BLUETOOTH')       
        self.wifi_checkbox.setChecked(self.parent.communication_style == 'WIFI')       
        
        self.yaw_sld.setValue(int(self.parent.yaw_sensibility))
        self.pitch_sld.setValue(int(self.parent.pitch_sensibility))
        self.yaw_label.setText('Yaw: ' + str(self.yaw_sld.value()/10))
        self.pitch_label.setText('Pitch: ' + str(self.pitch_sld.value()/10))

        self.pitch_sld.valueChanged[int].connect(self.pitchChangeValue)
        self.yaw_sld.valueChanged[int].connect(self.yawChangeValue)

    def WIFICheckboxClicked(self):
        """
        Callback for the wifi checkbox when clicked
        """
        if self.wifi_checkbox.checkState():
            self.bluetooth_checkbox.setChecked(False)
        else:
            self.bluetooth_checkbox.setChecked(True)

    def BLUETOOTHCheckboxClicked(self):
        """
        Callback for the wifi checkbox when clicked
        """
        if self.bluetooth_checkbox.checkState():
            self.wifi_checkbox.setChecked(False)
        else:
            self.wifi_checkbox.setChecked(True)

    def applyChanges(self):
        """
        Callback of the "Apply "buttons, here we save the parameters inside the GUI class (the parent window)
        """
        self.parent.IP = self.IP_input.text()
        self.parent.rasp_mac_addr = str(self.MAC_input.text())
        self.parent.min_speed = int(self.min_speed_input.text())
        self.parent.sld.setMinimum(self.parent.min_speed)
        self.parent.timeout = float(self.timeout_input.text())
        self.parent.yaw_sensibility = self.yaw_sensibility*10
        self.parent.pitch_sensibility = self.pitch_sensibility*10 # 10 is a scale factor
        if self.bluetooth_checkbox.checkState():
            self.parent.communication_style = 'BLUETOOTH'
        else:
            self.parent.communication_style = 'WIFI'

        self.close()

    def cancelBtn(self):
        """
        Close the window
        """
        self.close()

    def yawChangeValue(self, value):
        """
        Change value of the speed accordingly to the slidebar
        :param value:
        :return:
        """
        str_ = 'Yaw: ' + str(value/10)
        self.yaw_sensibility = value/10
        self.yaw_label.setText(str_)

    def pitchChangeValue(self, value):
        """
        Change value of the speed accordingly to the slidebar
        :param value:
        :return:
        """
        str_ = 'Pitch: ' + str(value/10)
        self.pitch_sensibility = value/10
        self.pitch_label.setText(str_)
